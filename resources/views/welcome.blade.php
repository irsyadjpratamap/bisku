<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Bisku') }}</title>

        <meta name="description" content="">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-select.css') }}">
        <link rel="stylesheet" href="{{ asset('css/scrollbar.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">
        <link rel="stylesheet" href="{{ asset('css/prettyPhoto.css') }}">
        <link rel="stylesheet" href="{{ asset('css/transitions.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('css/color.css') }}">
        <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
        <link rel="stylesheet" href="{{ asset('plugin/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
        <script src="{{ asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    </head>
    <body class="tg-home tg-homevone">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="loader">
            <div class="span">
                <div class="location_indicator"></div>
            </div>
        </div>

        <div class="loader">
            <div class="span">
                <div class="location_indicator"></div>
            </div>
        </div>

        <nav id="menu">
            <ul>
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Berita</a></li>
                <li><a href="#">Cek Pemesanan</a></li>
            </ul>
        </nav>

        <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
            <header id="tg-header" class="tg-header tg-haslayout">
                <div class="container-fluid">
                    <div class="row">
                        <div class="tg-topbar">
                            <nav class="tg-infonav">
                                <ul>
                                    <li>
                                        <i><img src="images/icons/icon-01.png" alt="image destinations"></i>
                                        <span>62858-2390-5201</span>
                                    </li>
                                    <li>
                                        <i><img src="images/icons/icon-02.png" alt="image destinations"></i>
                                        <span>PILIH TUJUAN ANDA <a href="javascript:void(0);">Temukan bus</a></span>
                                    </li>
                                </ul>
                            </nav>
                            <div class="tg-addnavcartsearch">
                                <nav class="tg-addnav">
                                    <ul>
                                        <li><a href="#">tentang</a></li>
                                        <li><a href="#">kontak</a></li>
                                    </ul>
                                </nav>
                                <nav class="tg-cartsearch">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);"><img src="images/icons/icon-03.png" alt="image destinations"></a>
                                            <div class="tg-cartitems">
                                                <div class="tg-cartlistitems">
                                                    <h3>Kotak Tiket</h3>
                                                    <div class="tg-cartitem">
                                                        <figure class="tg-itemimg"><img src="images/products/img-11.jpg" alt="image description"></figure>
                                                        <div class="tg-contentbox">
                                                            <div class="tg-producthead">
                                                                <em>x 2</em>
                                                                <h4><a href="javascript:void(0);">Bandung – Jogja<span>Senin Pagi</span></a></h4>
                                                            </div>
                                                            <span>Rp. 150.000</span>
                                                        </div>
                                                    </div>
                                                    <div class="tg-subtotal">
                                                        <h2>Subtotal</h2>
                                                        <span>Rp. 300.000</span>
                                                    </div>
                                                </div>
                                                <div class="tg-btnarea">
                                                    <a class="tg-btn" href="#"><span>lihat kotak</span></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li><a href="#tg-search"><img src="images/icons/icon-04.png" alt="image destinations"></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="tg-navigationarea tg-headerfixed">
                            <strong class="tg-logo"><a href="index.html"><img src="images/logo.png" alt="company logo here"></a></strong>
                            <div class="tg-socialsignin">
                                <ul class="tg-socialicons">
                                    <li><a href="javascript:void(0);"><i class="icon-facebook-logo-outline"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-instagram-social-outlined-logo"></i></a></li>
                                    <li><a href="javascript:void(0);"><i class="icon-twitter-social-outlined-logo"></i></a></li>
                                </ul>
                                @if (Route::has('login'))
                                <div class="tg-userbox">
                                    @auth
                                    <div class="dropdown tg-dropdown">
                                        <button class="tg-btndropdown" id="tg-dropdowndashboard" type="button" data-toggle="dropdown">
                                            <img src="images/author/img-01.jpg" alt="image description">
                                            <span>john smith</span>
                                            <i class="fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu tg-dropdownusermenu" aria-labelledby="tg-dropdowndashboard">
                                            <li><a href="dashboard.html">Dashboard</a></li>
                                            <li><a href="dashboard.html">Ubah Profile</a></li>
                                            <li><a href="javascript:void(0);">Keluar</a></li>
                                        </ul>
                                    </div>
                                    @else
                                    <a id="tg-btnsignin" class="tg-btn" href="#tg-loginsingup"><span>masuk</span></a>
                                    @endauth
                                </div>
                                @endif
                            </div>
                            <nav id="tg-nav" class="tg-nav">
                                <div class="navbar-header">
                                    <a href="#menu" class="navbar-toggle collapsed">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </a>
                                </div>
                                <div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
                                    <ul>
                                        <li><a href="#">beranda</a></li>
                                        <li><a href="#">berita</a></li>
                                        <li><a href="#">unduh</a></li>
                                        <li><a href="#">cek pemesanan</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>

            <div class="tg-bannerholder">
                <div class="tg-slidercontent">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>Booking ticket online</h1>
                                <h2>Bisku &mdash; Beli tiket bus dengan cepat dan mudah!</h2>
                                <form class="tg-formtheme tg-formtrip">
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="tg-select">
                                                <select class="selectpicker" data-live-search="true" data-width="100%" name="asal" id="asal">
                                                    <option>Asal</option>
                                                    <option value="Bandung">Bandung (Semua)</option>
                                                    <option value="Leuwi Panjang">Bandung (Leuwi Panjang)</option>
                                                    <option value="Cicaheum">Bandung (Cicaheum)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-select">
                                                <select class="selectpicker" id="tujuan" name="tujuan" data-live-search="true" data-width="100%">
                                                    <option>Tujuan</option>
                                                    <option value="Jakarta">Jakarta (Semua)</option>
                                                    <option value="Kp. Rambutan">Jakarta (Kp. Rambutan)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-select">
                                                <select class="selectpicker" id="bis" name="bis" data-live-search="true" data-width="100%">
                                                    <option>Bis</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="tg-dates">
                                                <div class="input-group">
                                                    <input type="text" id="keberangkatan" name="keberangkatan" class="form-control datepicker" placeholder="Keberangkatan">
                                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="cari tg-btn" type="button"><span>cari  bus</span></button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tg-homeslider" class="tg-homeslider owl-carousel tg-haslayout">
                    <figure class="item" data-vide-bg="poster: images/slider/img-01.jpg" data-vide-options="position: 0% 50%"></figure>
                    <figure class="item" data-vide-bg="poster: images/slider/img-02.jpg" data-vide-options="position: 0% 50%"></figure>
                    <figure class="item" data-vide-bg="poster: images/slider/img-03.jpg" data-vide-options="position: 0% 50%"></figure>
                </div>
            </div>

            <main id="tg-main" class="tg-main tg-haslayout">
                <section class="tg-sectionspace tg-haslayout">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tg-toursdestinations">
                                    <div class="tg-tourdestination tg-tourdestinationbigbox">
                                        <figure>
                                            <a href="javascript:void(0);">
                                                <img src="images/destination/img-01.jpg" alt="image destinations">
                                                <div class="tg-hoverbox">
                                                    <div class="tg-adventuretitle">
                                                        <h2>Promo 50% Mahasiswa</h2>
                                                    </div>
                                                    <div class="tg-description">
                                                        <p>your best vacation ever</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="tg-tourdestination">
                                        <figure>
                                            <a href="javascript:void(0);">
                                                <img src="images/destination/img-02.jpg" alt="image destinations">
                                                <div class="tg-hoverbox">
                                                    <div class="tg-adventuretitle">
                                                        <h2>Rute baru</h2>
                                                    </div>
                                                </div>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="tg-tourdestination">
                                        <figure>
                                            <a href="javascript:void(0);">
                                                <img src="images/destination/img-03.jpg" alt="image destinations">
                                                <div class="tg-hoverbox">
                                                    <div class="tg-adventuretitle">
                                                        <h2>Jadwal Baru</h2>
                                                    </div>
                                                </div>
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="tg-sectionspace tg-zerotoppadding tg-haslayout">
                    <div class="container">
                        <div class="row">
                            <div class="tg-features">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="tg-feature">
                                        <div class="tg-featuretitle">
                                            <h2><span>01</span>Luxury Hotels</h2>
                                        </div>
                                        <div class="tg-description">
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="tg-feature">
                                        <div class="tg-featuretitle">
                                            <h2><span>02</span>Tourist Guide</h2>
                                        </div>
                                        <div class="tg-description">
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="tg-feature">
                                        <div class="tg-featuretitle">
                                            <h2><span>03</span>Flights Tickets</h2>
                                        </div>
                                        <div class="tg-description">
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh tempor cum soluta nobis consectetuer nihil imperdiet doming...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="tg-parallax" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-03.jpg">
                    <div class="tg-sectionspace tg-haslayout">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="tg-ourpartners">
                                        <div class="tg-pattern"><img src="images/patternw.png" alt="image destination"></div>
                                        <h2>Mitra Kami</h2>
                                        <ul class="tg-partners">
                                            <li><figure><a href="javascript:void(0);"><img src="images/partners/img-01.png" alt="image destinations"></a></figure></li>
                                            <li><figure><a href="javascript:void(0);"><img src="images/partners/img-02.png" alt="image destinations"></a></figure></li>
                                            <li><figure><a href="javascript:void(0);"><img src="images/partners/img-03.png" alt="image destinations"></a></figure></li>
                                            <li><figure><a href="javascript:void(0);"><img src="images/partners/img-04.png" alt="image destinations"></a></figure></li>
                                            <li><figure><a href="javascript:void(0);"><img src="images/partners/img-05.png" alt="image destinations"></a></figure></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <footer id="tg-footer" class="tg-footer tg-haslayout">
                <div class="tg-fourcolumns">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="tg-footercolumn tg-widget tg-widgettext">
                                    <div class="tg-widgettitle">
                                        <h3>Tentang Bisku</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <div class="tg-description">
                                            <p>Bisku adalah platform pembelian dan pemesanan tiket bus secara online. Juga dapat mengecek jadwal bus.</p>
                                        </div>
                                        <span>1-800-321-6543</span>
                                        <a href="mailto:info@bisku.id">info@bisku.id</a>
                                        <ul class="tg-socialicons tg-socialiconsvtwo">
                                            <li><a href="javascript:void(0);"><i class="icon-facebook-logo-outline"></i></a></li>
                                            <li><a href="javascript:void(0);"><i class="icon-instagram-social-outlined-logo"></i></a></li>
                                            <li><a href="javascript:void(0);"><i class="icon-twitter-social-outlined-logo"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="tg-footercolumn tg-widget tg-widgetbuses">
                                    <div class="tg-widgettitle">
                                        <h3>Top Bus</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul>
                                            <li><a href="javascript:void(0);">Primajasa</a></li>
                                            <li><a href="javascript:void(0);">Pahala Kencana</a></li>
                                            <li><a href="javascript:void(0);">Arimbi</a></li>
                                            <li><a href="javascript:void(0);">MGI</a></li>
                                            <li><a href="javascript:void(0);">Damri</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="tg-footercolumn tg-widget tg-widgetdestinations">
                                    <div class="tg-widgettitle">
                                        <h3>Top Destinations</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <ul>
                                            <li><a href="javascript:void(0);">Jakarta</a></li>
                                            <li><a href="javascript:void(0);">Bandung</a></li>
                                            <li><a href="javascript:void(0);">Surabaya</a></li>
                                            <li><a href="javascript:void(0);">Yogyakarta</a></li>
                                            <li><a href="javascript:void(0);">Bali</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <div class="tg-footercolumn tg-widget tg-widgetnewsletter">
                                    <div class="tg-widgettitle">
                                        <h3>Newsletter</h3>
                                    </div>
                                    <div class="tg-widgetcontent">
                                        <div class="tg-description"><p>Daftar untuk informasi rute, bus dan promo terbaru</p></div>
                                        <form class="tg-formtheme tg-formnewsletter">
                                            <fieldset>
                                                <input type="email" name="email" class="form-control" placeholder="Your Email">
                                                <button type="submit"><img src="images/icons/icon-08.png" alt="image destinations"></button>
                                            </fieldset>
                                        </form>
                                        <span>We respect your privacy</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-footerbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <p>Copyright &copy; {{ date('Y') }} Bisku. All  rights reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <div id="tg-search" class="tg-search">
            <button type="button" class="close"><i class="icon-cross"></i></button>
            <form>
                <fieldset>
                    <div class="form-group">
                        <input type="search" name="search" class="form-control" value="" placeholder="cari bus atau destinasi">
                        <button type="submit" class="tg-btn"><span class="icon-search2"></span></button>
                    </div>
                </fieldset>
            </form>
            <ul class="tg-destinations">
                <li>
                    <a href="javascript:void(0);">
                        <h3>Europe</h3>
                        <em>(05)</em>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <h3>Africa</h3>
                        <em>(15)</em>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <h3>Asia</h3>
                        <em>(12)</em>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <h3>Oceania</h3>
                        <em>(02)</em>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <h3>North America</h3>
                        <em>(08)</em>
                    </a>
                </li>
            </ul>
        </div>

        <div id="tg-loginsingup" class="tg-loginsingup" data-vide-bg="poster: images/singup-img.jpg" data-vide-options="position: 0% 50%">
            <div class="tg-contentarea tg-themescrollbar">
                <div class="tg-scrollbar">
                    <button type="button" class="close">x</button>
                    <div class="tg-logincontent">
                        <nav id="tg-loginnav" class="tg-loginnav">
                            <ul>
                                <li><a href="#">Tentang Kami</a></li>
                                <li><a href="#">Hubungi Kami</a></li>
                                <li><a href="#">Akun Saya</a></li>
                                <li><a href="#">Daftar Belanja</a></li>
                            </ul>
                        </nav>
                        <div class="tg-themetabs">
                            <ul class="tg-navtbs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" data-toggle="tab">Telah Mendaftar</a></li>
                                <li role="presentation"><a href="#profile" data-toggle="tab">Belum Mendaftar</a></li>
                            </ul>
                            <div class="tg-tabcontent tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="home">
                                    <form class="tg-formtheme tg-formlogin" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label>Surel <sup>*</sup></label>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Password <sup>*</sup></label>
                                                <input id="password" type="password" class="form-control" name="password" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="tg-checkbox">
                                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label for="remember">Ingat Saya</label>
                                                </div>
                                                <span><a href="{{ route('password.request') }}">Lupa password?</a></span>
                                            </div>
                                            <button type="submit" class="login tg-btn tg-btn-lg"><span>Masuk</span></button>
                                        </fieldset>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <form class="tg-formtheme tg-formlogin" method="POST" action="{{ route('register') }}">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <div class="form-group">
                                                <label>Nama Depan <sup>*</sup></label>
                                                <input type="text" name="firstname" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Belakang <sup>*</sup></label>
                                                <input type="text" name="firstname" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>No. HP <sup>*</sup></label>
                                                <input type="text" name="firstname" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Surel <sup>*</sup></label>
                                                <input type="text" name="firstname" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat <sup>*</sup></label>
                                                <textarea class="form-control" rows="5"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Password <sup>*</sup></label>
                                                <input type="password" name="password" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Confirm Password <sup>*</sup></label>
                                                <input type="password" name="password" class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <div class="tg-checkbox">
                                                    <input type="checkbox" name="agree" id="agree">
                                                    <label for="agree">Setuju dengan <a href="#">syarat dan ketentuan</a> berlaku</label>
                                                </div>
                                            </div>
                                            <button class="tg-btn tg-btn-lg"><span>Daftar</span></button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="tg-shareor"><span>atau</span></div>
                        <div class="tg-signupwith">
                            <h2>Masuk Dengan...</h2>
                            <ul class="tg-sharesocialicon">
                                <li class="tg-facebook"><a href="#"><i class="icon-facebook-1"></i><span>Facebook</span></a></li>
                                <li class="tg-twitter"><a href="#"><i class="icon-twitter-1"></i><span>Twitter</span></a></li>
                                <li class="tg-googleplus"><a href="#"><i class="icon-google4"></i><span>Google+</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/vendor/jquery-library.js') }}"></script>
        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('js/jquery-scrolltofixed.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/jquery.mmenu.all.js') }}"></script>
        <script src="{{ asset('js/packery.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/jquery.vide.min.js') }}"></script>
        <script src="{{ asset('js/scrollbar.min.js') }}"></script>
        <script src="{{ asset('js/prettyPhoto.js') }}"></script>
        <script src="{{ asset('js/countdown.js') }}"></script>
        <script src="{{ asset('js/parallax.js') }}"></script>
        <script src="{{ asset('js/gmap3.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('plugin/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script>
            $('.datepicker').datepicker({
                autoclose: true
            });
        </script>
        <script src="{{ asset('js/custom/welcome.js') }}"></script>
    </body>
</html>
